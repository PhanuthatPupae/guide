import React, { Component } from "react";
import {
  Card,
  Comment,
  Avatar,
  List,
  Tooltip,
  Rate,
  Button,
  Modal,
  Form,
  Input,
  message
} from "antd";
import { Element, animateScroll as scroll } from "react-scroll";
import { withRouter } from "react-router-dom";
import axios from "axios";
import moment from "moment";
import { connect } from "react-redux";
const TextArea = Input.TextArea;
const desc = [1, 2, 3, 4, 5];
class Review extends Component {
  state = {
    star: 0,
    comment: [],
    review: [],
    id: "",
    isClick: false,
    reviewDataByShopAndCustomer: [],
    user: {
      id: 0
    }
  };
  componentWillMount() {
    const { id } = this.props;
    this.setState({ id });
    this.setState({ user: this.props.UserInfo.user });
    this.callReviewAll();
  }
  callReviewAll = () => {
    const { id } = this.props;
    axios
      .get(`http://3.88.222.254:8888/reviews/shop/${id}`)
      .then(review => {
        console.log(
          "review ====================================================>",
          review.data
        );

        this.props.OnLoadReview(review.data);
        this.forceUpdate();
      })
      .catch(error => {
        console.log("error" + error);
      });
  };

  OnClickEdit = reviewDataByShopAndCustomer => {
    const { user } = this.props.UserInfo;
    if (user.id !== reviewDataByShopAndCustomer.customer.id) {
      this.setState({ isClick: false });
    } else {
      this.setState({ isClick: true });
      this.setState({ reviewDataByShopAndCustomer });
    }
  };
  onChange = () => {
    this.props.history.push("/map");
  };

  OnModalCancel = () => {
    this.setState({ isClick: false });
  };
  commentChange = event => {
    const comments = event.target.value;
    console.log("comments", comments);
    this.setState({ comments });
  };

  starChange = event => {
    const star = event;
    console.log("comments", star);
    this.setState({ star });
  };
  CallUpdateComment = () => {
    const { id, customer } = this.state.reviewDataByShopAndCustomer;
    const { comments, star } = this.state;

    axios
      .put(
        `http://3.88.222.254:8888/update/reviews/${id}/customer/${customer.id}`,
        {
          review: comments,
          start: star,
          shopID: this.state.id
        }
      )
      .then(res => {
        console.log(
          "res page review OnclickOk Modal =======================>",
          res
        );
      })
      .then(() => {
        message.success("Edit Success", 1);
        this.callReviewAll()
      })
      // .then(() => {

      // })
      .catch(error => {
        console.log(
          "ERROR page review OnclickOk Modal==================>",
          error
        );
      });
  };
  OnCickOk = () => {
    this.setState({ isClick: false });
    this.CallUpdateComment();
  };

  render() {
    const { review } = this.props.Review;
    const { customer } = this.state.reviewDataByShopAndCustomer;

    console.log("props review =======>", this.props.Review);

    return (
      <div>
        <Modal
          title="แก้ไขรีวิว"
          visible={this.state.isClick}
          onOk={() => {
            this.OnCickOk();
          }}
          onCancel={() => {
            this.OnModalCancel();
          }}
        >
          <Comment
            avatar={
              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
            }
            content={
              <div>
                <Form.Item>
                  <TextArea rows={4} onChange={this.commentChange} />
                </Form.Item>
                <Form.Item>
                  <div style={{ textAlign: "left", alignItems: "left" }}>
                    <p>
                      ความพอใจ :{" "}
                      <Rate tooltips={desc} onChange={this.starChange} />
                    </p>
                  </div>
                </Form.Item>
              </div>
            }
          />
        </Modal>
        <Card style={{ width: "100%", height: "550px" }}>
          <Element
            style={{
              position: "relative",
              height: "500px",
              overflow: "scroll",
              marginBottom: "100px"
            }}
          >
            <List
              header={`${review.length} ${
                review.length > 1 ? "replies" : "reply"
              }`}
              itemLayout="horizontal"
              dataSource={review}
              renderItem={item => (
                <List.Item>
                  <Comment
                    author={<a>{item.customer.email}</a>}
                    avatar={
                      <Avatar
                        src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                        alt={item.customer.email}
                      />
                    }
                    actions={<a>{item.customer.email}</a>}
                    content={
                      <div>
                        <p style={{ textAlign: "left", textIndent: "50px" }}>
                          {item.review}
                        </p>
                        <div>
                          <p>
                            ความพอใจ : <Rate value={item.start} disabled />
                          </p>
                        </div>
                        <div
                          style={{
                            alignItems: "left",
                            justifyContent: "left",
                            alignContent: "left",
                            paddingRight: "150px"
                          }}
                        >
                          {this.state.user === undefined ? (
                            ""
                          ) : (
                            <Button
                              type="dashed"
                              size="small"
                              onClick={() => {
                                this.OnClickEdit(item);
                              }}
                            >
                              แก้ไข
                            </Button>
                          )}
                        </div>
                      </div>
                    }
                    datetime={
                      <Tooltip title={moment().format("YYYY-MM-DD HH:mm:ss")}>
                        <span>{moment().fromNow()}</span>
                      </Tooltip>
                    }
                  />
                </List.Item>
              )}
            />
          </Element>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log("state =====================> mapstate", state.review);
  return {
    Category: state.Category,
    UserInfo: state.UserInfo,
    Review: state.Review
  };
};
const mapDispatchToProps = dispatch => {
  return {
    OnLoadReview: item =>
      dispatch({
        type: "LOAD_REVIEW",
        loadReview: item
      })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Review);
