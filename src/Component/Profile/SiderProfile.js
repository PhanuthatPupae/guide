import React, { Component } from "react";
import { Menu, Icon, Layout, message } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
const { Sider } = Layout;
const { SubMenu } = Menu;
const login = ["login", "logout"];

class SiderProfile extends Component {
  state = {
    currentPath: ""
  };
  OnClick = pathSend => {
    var path = "/";
    path = `/${pathSend.key}`;
    this.setState({ currentPath: pathSend.key });
    this.props.history.replace(path);
  };
  onClickLogout = () => {
    const { token } = this.props.UserInfo;
    console.log(
      "prop =====================================================",
      token
    );
    if (token !== null) {
      this.props.onLogout();
      message.success("Log out sucess", 1, () => {
        window.location.replace("/home");
        // this.props.history.push("/home");
      });
    } else {
      console.log(
        "prop =====================================================",
        token
      );
    }
  };
  render() {
    return (
      <div>
        <Sider
          width={200}
          style={{
            background: "#fff",
            paddingTop: "70px",
            marginBottom: "22px",
            height: "100%"
          }}
        >
          <Menu
            mode="inline"
            SelectedKeys={[this.state.currentPath]}
            defaultSelectedKeys={[this.state.currentPath]}
            //   // defaultOpenKeys={["category"]}
            style={{ height: "100%", borderRight: 0, textAlign: "left" }}
          >
            <Menu.Item key="profile">
              <Icon
                type="user"
                onClick={key => {
                  this.OnClick(key);
                }}
              />
              ข้อมูลส่วนตัว
            </Menu.Item>

            <Menu.Item
              key="home"
              onClick={key => {
                this.OnClick(key);
              }}
            >
              <Icon type="home" />
              หน้าแรก
            </Menu.Item>
            <Menu.Item
              key="map"
              onClick={key => {
                this.OnClick(key);
              }}
            >
              <Icon type="environment" />
              แผนที่
            </Menu.Item>
            <Menu.Item
              key="edit"
              onClick={key => {
                this.OnClick(key);
              }}
            >
              <Icon type="edit" />
              แก้ไขข้อมูลส่วนตัว
            </Menu.Item>

            <Menu.Item key={login[0]} onClick={path => this.OnClick(path)}>
              <Icon type="login" />
              ลงชื่อเข้าใช้
            </Menu.Item>
            <Menu.Item onClick={() => this.onClickLogout()}>
              <Icon type="logout" />
              ออกจากระบบ
            </Menu.Item>
          </Menu>
        </Sider>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    Category: state.Category,
    ShopList: state.ShopList,
    UserInfo: state.UserInfo
  };

};
const mapDispatchToProps = dispatch => {
  return {
  
    onLogout: items =>
      dispatch({
        type: "LOG_OUT",
        logout: items
      })
  };
};
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(SiderProfile));
