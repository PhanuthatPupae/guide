import React, { Component } from "react";
import { Menu, Icon, Layout, Row, Col, Avatar } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
const { Header } = Layout;
const menus = ["home", "map"];
class HeaderBar extends Component {
  state = {
    currentMenu: menus[0]
  };
  onMenuClick = e => {
    var path = "/";
    path = `/${e.key}`;
    console.log("this.props", this.props);
    this.props.history.replace(path);

    console.log(" path", path);
    this.setState({ currentMenu: path });
  };

  render() {
    console.log(
      "header this.prop.UserInfo ======>",
      this.props.UserInfo.state.length === 0
    );
    return (
      <div>
        <Header
          className="header"
          style={{
            position: "fixed",
            zIndex: 1,
            width: "100%",
            background: "#E0BBE4",
            paddingRight: "600px"
          }}
        >
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            SelectedKeys={[this.state.currentMenu]}
            defaultSelectedKeys={menus[0]}
            style={{ lineHeight: "64px", background: "#E0BBE4" }}
            onClick={menubar => {
              this.onMenuClick(menubar);
            }}
          >
            <Menu.Item key={menus[0]}>
              <Icon type="home" />
              หน้าแรก
            </Menu.Item>
            <Menu.Item key={menus[1]}>
              <Icon type="environment" />
              แผนที่
            </Menu.Item>
      
          </Menu>
          <div style={{ position: "absolute", right: "1px", top: "2px" }}>
            {/* <Row>
              <Col span={12}>
                {" "}
                <Avatar
                  style={{ backgroundColor: "#87d068", paddingRight: "15px" }}
                >
                  {" "}
                  U
                </Avatar>
              </Col>
              <Col span={24}>
                <div>User name </div>
              </Col>
              <Col span={8}>
                {this.props.UserInfo.state.length === 0
                  ? "User"
                  : this.props.UserInfo.user.email}
              </Col>
            </Row> */}
          </div>
        </Header>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    UserInfo: state.UserInfo
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    null
  )(HeaderBar)
);
