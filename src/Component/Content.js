import React, { Component } from "react";
import ShopList from "./ShopList";
import { Layout } from "antd";
import { withRouter } from "react-router-dom";
import RouteHeader from '../Container/RouteHeader'
import { connect } from "react-redux";
const { Content } = Layout;
class Contents extends Component {

  render() {  
   
    return (
      <div>
        <Content
          style={{
            background: "#fff",
            padding: 0,
            margin: 0,
            minHeight: 1000
          }}
        >
         <RouteHeader/>
        </Content>
      </div>
    );
  }
}
export default withRouter(connect(null,null)(Contents));