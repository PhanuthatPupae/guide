import { applyMiddleware, createStore, combineReducers, compose } from "redux";
import logger from "redux-logger";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web and AsyncStorage for react-native
import category from "../Reducers/Category";
import shopList from "../Reducers/ShopLists";
import userInfo from "../Reducers/UserInfo";
import review from "../Reducers/Review";
const persistConfig = {
  key: "root",
  storage
};

const reducers = () =>
  combineReducers({
    Category: category,
    ShopList: shopList,
    UserInfo: userInfo,
    Review:review
   
  });
const persistedReducer = persistReducer(persistConfig, reducers());

export const store = createStore(
  persistedReducer,
  compose(applyMiddleware(logger))
);
export const persistor = persistStore(store);
