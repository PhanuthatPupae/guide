import React, { Component } from "react";
import { Layout, Col, Carousel } from "antd";
import { withRouter } from "react-router-dom";
import Content from "../Component/Content";
import { connect } from "react-redux";
import Bar from "../Component/Header";
import SiderBar from "../Component/Sider";
import Footer from "../Component/Footer";

class Main extends Component {
  componentWillMount() {
    console.log("props", this.props);
  }
  render() {
    console.log("props", this.props);
    return (
      <div>
        <Layout style={{ background: "red", backgroundColor: "red" }}>
          <Bar />
          <Layout>
            <SiderBar />
            <Layout style={{ padding: "0 24px 24px" }}>
              
              <Content/>
            </Layout>
          </Layout>
          <Footer />
        </Layout>
      </div>
    );
  }
}
export default withRouter(
  connect(
    null,
    null
  )(Main)
);
