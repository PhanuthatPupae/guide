export default (
  state = {
    data: [
      {
        id: "",
        category: "",
        lang: {
          th: {},
          en: {}
        },
        location: {},
        image: ""
      }
    ]
  },
  action
) => {
  switch (action.type) {
    case "SHOP_LIST":
      return {
        data: action.load
      };

    default:
      return state;
  }
};
