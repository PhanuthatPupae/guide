export default (
  state = {
    modalVisible: false,
    modal: {}
  },
  action
) => {
  switch (action.type) {
    case "CLICK_COMMENT":
      return {
        modalVisible: true,
        modal: action.payload
      };
    case "DISMISS_DIALOG":
      return {
        modalVisible: false,
        modal:  {}
      };

    default:
      return state;
  }
};
